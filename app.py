from tkinter import filedialog
from os import listdir
from re import match, findall
import xlrd


class ExcelReader:
    excel = None
    sheet = None

    def __init__(self, file):
        self.excel = xlrd.open_workbook(file)

    def sheet_by_index(self, index):
        self.sheet = self.excel.sheet_by_index(index)

    def sheet_by_name(self, name):
        self.sheet = self.excel.sheet_by_name(name)

    @staticmethod
    def parse(string):
        col_str = findall('^[A-Z]+', string)[0]
        row_str = findall('[0-9]+$', string)[0]
        row_index = int(row_str) - 1
        col_index = 0
        index = 0
        for s in col_str:
            col_index += (ord(s) - 65) + 26 * index
            index += 1
        return row_index, col_index

    def cell(self, string):
        row, col = self.parse(string)
        return self.sheet.cell(row, col)


def process_file(_files):
    _data = []
    for _excel_file in _files:
        excel = ExcelReader(_excel_file)
        excel.sheet_by_index(0)
        _dict = {'Z1': excel.cell('Z1').value}
        _data.append(_dict)
    return _data


_dir = filedialog.askdirectory(initialdir='D:\\')
_file_list = listdir(_dir)
_excel_files = []
for _file in _file_list:
    if match('^(.)+(xlsx)$', _file):
        _excel_files.append(_dir + '\\' + _file)
_proc_data = process_file(_excel_files)
print(_proc_data)
